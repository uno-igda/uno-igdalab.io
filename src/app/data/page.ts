export interface Page {
    title:string;
    body: string;

}

export interface PageMap{
    [slug:string] : Page
}

export const DATA_PAGES:PageMap = {

    'phaser-setup-2018' : {
        title:'Getting Setup For Phaser.js Game Development',
        body: `
        <div class="container">
            <h1>Getting Setup For Phaser.js Game Development</h1>
            <h4>Lesson 0 - Game Development With Phaser</h4>
            <p>In this series we are going to take you from 0 to game developer using the JavaScript Game Framework
                called Phaser. You will learn programming, level design and how to create 2D pixel-art. Please click on
                the links below and checkout each tool we will be using, the ones that you need to download or install are labled with: (install/download)
            </p>
            <h4>Tools Needed</h4>
            <ul>
                <li><a href="http://phaser.io/download/stable" target="_blank">Phaser</a> (download "js version")</li>
                <li><a href="https://www.mapeditor.org/download.html" target="_blank">Tiled</a> (install)</li>
                <li><a href="https://www.piskelapp.com/" target="_blank">Piskel</a></li>
                <li><a href="https://www.google.com/chrome/" target="_blank">Chrome</a> (install)</li>
                <li><a href="https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb?hl=en" target="_blank">Chrome extension web server</a> (install - *chrome required)</li>
            </ul>

            <span>Next: </span><a class="btn btn-small disabled" href="#">Lesson 1 - Initializing a Phaser Game</a>
        </div>
        `
    },

    'phaser-2018-lesson01' : {
        title:'Game Development in Phaser.js - Lesson 01, Initializing a Phaser Game',
        body: `
        <div class="container">
            <h1>Game Development in Phaser.js</h1>
            <h4>Lesson 01 -  Initializing a Phaser Game</h4>
            <p>In this lessson you will create the structure for this project and write code to initialize your first
            Phaser game.</p>
            
            <iframe width="560" height="315" src="https://www.youtube.com/embed/oanvZ20MS2Q" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            <br>
            <span>Next: </span><a class="btn btn-small disabled" href="#">Lesson 2</a><span>(Comming Sooon)</span>
        </div>
        `
    }
}

export const DATA_LESSONS = {

    "Game Development In Phaser - 2018": {
        pages: [
            'phaser-setup-2018',
            'phaser-2018-lesson01'
        ]
    }
}