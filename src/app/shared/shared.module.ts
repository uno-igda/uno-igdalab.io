import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreviewCardComponent } from './preview-card/preview-card.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PreviewCardComponent
  ],
  exports: [
    PreviewCardComponent
  ]
})
export class SharedModule { }
