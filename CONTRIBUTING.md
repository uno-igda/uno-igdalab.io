# Steps for Contributing

1. Clone this repo. `git clone https://gitlab.com/uno-igda/uno-igda.gitlab.io.git`
2. Checkout **development** branch. `git checkout development`
3. Create a sub branch of development. `git checkout -b your-new-feature`
3. Make changes.
4. Commit changes and push.
5. from the gitlab interface submit a merge request with branch development.