import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PageArcadeComponent } from './page-arcade/page-arcade.component';
import { PagePeopleComponent } from './page-people/page-people.component';
import { PageLessonsComponent } from './page-lessons/page-lessons.component';
import { DnyamicPageComponent } from './dnyamic-page/dnyamic-page.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'arcade', component: PageArcadeComponent },
  { path: 'people', component: PagePeopleComponent },
  { path: 'lessons', component: PageLessonsComponent },
  { path: 'p/:pageId', component: DnyamicPageComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes,{useHash: true}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
