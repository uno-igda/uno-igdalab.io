import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JumbotronComponent } from './jumbotron/jumbotron.component';
import { MeetupComponent } from './meetup/meetup.component';
import { HomeComponent } from './home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { SharedModule } from '../shared/shared.module';
import { PreviewCardComponent } from '../shared/preview-card/preview-card.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatButtonModule,
    SharedModule,
  ],
  declarations: [
    HomeComponent,
    JumbotronComponent,
    MeetupComponent,
  ]
})
export class HomeModule { }
