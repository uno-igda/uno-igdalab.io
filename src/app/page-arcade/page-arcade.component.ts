import { Component, OnInit } from '@angular/core';
import { DATA_gamelinks, GameLink } from '../data/gamelinks';
import { PreviewCard, PreviewCardButton } from '../shared/preview-card/preview-card.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-page-arcade',
  templateUrl: './page-arcade.component.html',
  styleUrls: ['./page-arcade.component.css']
})
export class PageArcadeComponent implements OnInit {

  games:PreviewCard[];

  constructor(
    private router:Router
  ) {
    this.games = this.previewcardsFromGameLinks(DATA_gamelinks);
  }

  ngOnInit() {
  }

  previewcardsFromGameLinks(games:GameLink[]):PreviewCard[]{
    let cards:PreviewCard[] = new Array<PreviewCard>();

    games.forEach(game => {
      let card = new PreviewCard();
      card.text = `${game.title} - ${game.description}`;
      card.note = game.author;
      card.imgSrc = null;//game.img

      let buttons:PreviewCardButton[] = [
        new PreviewCardButton('Play', ()=>{
          console.log('play game')
        }),
      ]
      card.buttons = buttons;

      cards.push(card)
    });

    return cards;
  }
  
  

}
