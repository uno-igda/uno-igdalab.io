import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageArcadeComponent } from './page-arcade.component';

describe('PageArcadeComponent', () => {
  let component: PageArcadeComponent;
  let fixture: ComponentFixture<PageArcadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageArcadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageArcadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
