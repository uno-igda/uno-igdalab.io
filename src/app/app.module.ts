import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { PreviewCardComponent } from './shared/preview-card/preview-card.component';
import {MatButtonModule} from '@angular/material/button';
import { AppRoutingModule } from './app-routing.module';
import { HomeModule } from './home/home.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FooterComponent } from './shared/footer/footer.component';
import { SharedModule } from './shared/shared.module';
import { PageLessonsComponent } from './page-lessons/page-lessons.component';
import { PagePeopleComponent } from './page-people/page-people.component';
import { PageArcadeComponent } from './page-arcade/page-arcade.component';
import { PageDirective } from './page.directive';
import { DnyamicPageComponent } from './dnyamic-page/dnyamic-page.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PageNotFoundComponent,
    FooterComponent,
    PageLessonsComponent,
    PagePeopleComponent,
    PageArcadeComponent,
    PageDirective,
    DnyamicPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    AppRoutingModule,
    HomeModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
