import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PageDirective } from '../page.directive';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { DATA_PAGES, Page } from '../data/page';
import { ComponentFactoryResolver } from '@angular/core/src/render3';

@Component({
  selector: 'app-dnyamic-page',
  templateUrl: './dnyamic-page.component.html',
  styleUrls: ['./dnyamic-page.component.css']
})
export class DnyamicPageComponent implements OnInit {

  @ViewChild('pagebody') private pagebody: ElementRef;
  constructor(
    private route: ActivatedRoute
    ) { }

  currentPageIndex:string;
  page:Page = {
    body: '',
    title:''
  }
  
  ngOnInit() {
    this.loadComponent()
  }

  loadComponent() {

    this.route.paramMap.subscribe(
      (param:ParamMap) => {
        this.currentPageIndex = param.get('pageId');
        this.page = DATA_PAGES[this.currentPageIndex]
        this.pagebody.nativeElement.innerHTML = this.page.body
      }
    );
  }

}
