import { Component, OnInit, Input } from '@angular/core';
import { Button } from 'protractor';

export class PreviewCard{
  text:string = '';
  note:string = '';
  maxWidth:string = '100%';
  imgHeight:string = '225px';
  imgSrc:string = null;
  buttons:PreviewCardButton[] = null;
}

export class PreviewCardButton{
  constructor(text:string, action: ()=> void){
    this.text = text; this.action = action;
  }
  text:string;
  action: () => void;
}

@Component({
  selector: 'preview-card',
  templateUrl: './preview-card.component.html',
  styleUrls: ['./preview-card.component.css']
})
export class PreviewCardComponent implements OnInit {

  defButtons:PreviewCardButton[] = [
    {text:'view', action: () => {
      console.log("This is the default card.")
    }}
  ]

  @Input() card: PreviewCard = {
    text:'This is a parapgraph used to descrive the contents of the card this item represents. It can be long or short, its up to you.',
    note:'Free',
    maxWidth:'100%',
    imgHeight:'225px',
    imgSrc: null,
    buttons: this.defButtons,
  }
  

  constructor() { }

  ngOnInit() {
  }

}
