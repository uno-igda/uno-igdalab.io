import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageLessonsComponent } from './page-lessons.component';

describe('PageLessonsComponent', () => {
  let component: PageLessonsComponent;
  let fixture: ComponentFixture<PageLessonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageLessonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageLessonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
