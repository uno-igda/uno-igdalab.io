import { Component, OnInit } from '@angular/core';
import { Person, DATA_people } from '../data/people';

@Component({
  selector: 'app-page-people',
  templateUrl: './page-people.component.html',
  styleUrls: ['./page-people.component.css']
})
export class PagePeopleComponent implements OnInit {

  constructor() { }

  people:Person[] = DATA_people;

  ngOnInit() {
  }

}
