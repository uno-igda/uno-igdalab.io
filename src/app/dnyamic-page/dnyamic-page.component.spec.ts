import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DnyamicPageComponent } from './dnyamic-page.component';

describe('DnyamicPageComponent', () => {
  let component: DnyamicPageComponent;
  let fixture: ComponentFixture<DnyamicPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DnyamicPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DnyamicPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
