import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[Page]'
})
export class PageDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
