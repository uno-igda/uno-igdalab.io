/**
 * THis file defines the schema and data for game links used on the arcade page.
 */
export class GameLink {
    constructor(title:string, description:string, img:string, link:string, author:string){
      this.title = title;
      this.description=description;
      this.img = img;
      this.link = link;
      this.author = author;
    }
    title:string;//title of game
    description:string;//description of game
    img:string;//path to image
    link:string;//link to game
    author:string;
  }

export const DATA_gamelinks:GameLink[] = [
    new GameLink("game 1", "some description", "/assets/img/games/game1.jpg", "www.google.com" , 'some one'),
    new GameLink("game 2", "some description", "/assets/img/games/game1.jpg", "www.google.com" , 'some one'),
    new GameLink("game 3", "some description", "/assets/img/games/game1.jpg", "www.google.com" , 'some one'),
    new GameLink("game 4", "some description", "/assets/img/games/game1.jpg", "www.google.com" , 'some one'),
    new GameLink("game 5", "some description", "/assets/img/games/game1.jpg", "www.google.com" , 'some one'),
    new GameLink("game 6", "some description", "/assets/img/games/game1.jpg", "www.google.com" , 'some one'),
]