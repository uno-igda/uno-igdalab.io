export class Person {
    constructor(name:string,title:string, email:string, bio:string, imgUrl:string){
        this.name = name;
        this.title = title;
        this.email = email;
        this.bio = bio;
        this.imgUrl = imgUrl;
    }
    name:string;
    title:string;
    email:string;
    bio:string;
    imgUrl:string;
}

export const DATA_people = [
    new Person("Nolan Sherman", "President", "nsherma1@uno.edu", "", "/src/assets/img/nolan.jpg"),
    new Person("Christina Bui", "Vice President", "cbui@uno.edu", "", null),
    new Person("Stephen Ware", "Faculty Sponsor", "sgware@uno.edu", "", null),
    new Person("Ted Holmberg", "Faculty Sponsor", "eholmber@uno.edu", "", null),
]